require 'yaml'

CDC_ROOT = File.dirname(__FILE__)

def cdc_path(path)
  File.expand_path(path, CDC_ROOT)
end

CONFIG ||= YAML.load_file(cdc_path('config.yaml'))

def cdc_path_db
  cdc_path_of_config(:db)
end

def film_dir(id)
  format('%s/film/%s', cdc_path_of_config(:src), id)
end

def cdc_path_issue_xml(number)
  format('%s/issue/%03i.xml', cdc_path_of_config(:src), number)
end

def cdc_path_reviewers_xml
  format('%s/reviewers.xml', cdc_path_of_config(:src))
end

def cdc_path_completed
  cdc_path_of_config(:completed)
end

def cdc_path_index_year_template
  cdc_path_of_config(:index_year)
end

def cdc_path_bib_year_template
  cdc_path_of_config(:bib_year)
end

def cdc_path_bibtool_res
  cdc_path_of_config(:bibtool_res)
end

def cdc_path_bib(year)
  format('%s/cdc%i.bib', cdc_path_of_config(:bib), year)
end

def require_cdc(path)
  require cdc_path(['lib', path].join('/'))
end

private

def cdc_path_of_config(key)
  cdc_path(CONFIG[:paths][key])
end
