require 'set'

class CdC::Film < Sequel::Model(:films)

  many_to_many(:reviews, join_table: :film_reviews)
  one_to_many(:imdbs, class: 'CdC::IMDb')
  one_to_many(:aocs, class: 'CdC::AoC')

  plugin :validation_helpers

  def validate
    super
  end

  def directors
    [director1, director2, director3, director4].compact
  end

  def directors_attribution
    text =
      if directors.count == 1 then
        directors.first
      else
        directors_copy = directors
        last = directors_copy.pop
        [directors_copy.join(', '), last].join(' et ')
      end
    attribute(text)
  end

  def title_lang
    super.to_sym
  end

  # We group film objects (which may have different titles in different
  # reviews) by the (real) film's slug, for that we use the IMDb/AoC ids,
  # but if the film has neither we drop back to the film object's DB id
  # which will at least be unique.  It should be very rare to have a
  # film with multiple instances but no IMDb/AoC ids ...

  def slug
    parts =
      [imdb_ids, aoc_ids]
        .map(&:sort)
        .flatten
    if parts.empty? then
      "film-id:#{id}"
    else
      parts.join(':')
    end
  end

  def sort_key(n)
    key =
      title
      .gsub(/«|»/, '')
      .sub(definite_article_regexp, '')
      .strip
    if key =~ /^([0-9]+)(.*)$/ then
      key = format('%08i%s', $1.to_i, $2)
    end
    format('%s %08i', I18n.transliterate(key).downcase, n)
  end

  private

  def imdb_ids
    imdbs.map(&:imdb_id)
  end

  def aoc_ids
    aocs.map(&:aoc_id)
  end

  NON_MATCH_REGEXP = /(?!)/

  DEFINITE_ARTICLES_REGEXP = {
    en: /^(The )/,
    fr: /^(Les |La |Le |L’)/,
    es: /^(El |La |Los |Las )/,
    it: /^(Il |Lo |L’|I |Gli |La |Le )/,
    de: /^(Der |Die |Das |Den |Dem |Des )/,
    se: /^(Det |Den |De )/,
    pt: /^(O |Os |A |As )/,
    cs: NON_MATCH_REGEXP,
    nl: /^(De |Het )/,
    da: NON_MATCH_REGEXP,
    hu: /^(A |Az )/,
    ru: NON_MATCH_REGEXP,
    jp: NON_MATCH_REGEXP,
    # there are several of these, add as needed
    gr: /^(To )/,
    pl: NON_MATCH_REGEXP,
    fi: NON_MATCH_REGEXP,
    cz: NON_MATCH_REGEXP,
    # Bengail does have articles, but always after the subject
    bn: NON_MATCH_REGEXP,
    ar: /^(Al-)/,
  }.freeze

  def definite_article_regexp
    DEFINITE_ARTICLES_REGEXP.fetch(title_lang)
  end

  VOWELS = Set['A', 'À', 'E', 'É', 'H', 'I', 'O', 'U'].freeze

  def attribute(text)
    if VOWELS.include? text.chars.first.upcase then
      "d’#{text}"
    else
      "de #{text}"
    end
  end
end
