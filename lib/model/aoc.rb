class CdC::AoC < Sequel::Model(:aocs)

  many_to_one(:film)

  plugin :validation_helpers

  def validate
    super
  end

end
