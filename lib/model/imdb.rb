class CdC::IMDb < Sequel::Model(:imdbs)

  many_to_one(:film)

  plugin :validation_helpers

  def validate
    super
  end

end
