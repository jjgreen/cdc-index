class CdC::Issue < Sequel::Model(:issues)

  unrestrict_primary_key
  one_to_many(:reviews, key: :number)

  plugin :validation_helpers

  def validate
    validates_presence [:number, :month_first, :month_last, :year]
    validates_includes (1..12), :month_first
    validates_includes (1..12), :month_last
    super
  end

  def formatted_month
    if month_first == month_last then
      MONTHS_FR[month_first]
    else
      [month_first, month_last].map { |m| MONTHS_FR[m] }.join('/')
    end
  end

  MONTHS_FR = {
    1  => 'janvier',
    2  => 'février',
    3  => 'mars',
    4  => 'avril',
    5  => 'mai',
    6  => 'juin',
    7  => 'juillet',
    8  => 'août',
    9  => 'septembre',
    10 => 'octobre',
    11 => 'novembre',
    12 => 'décembre'
  }.freeze

end
