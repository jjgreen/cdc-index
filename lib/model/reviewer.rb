require 'any_ascii'

class CdC::Reviewer < Sequel::Model(:reviewers)

  many_to_many(:reviews, join_table: :reviewer_reviews)

  plugin :validation_helpers

  def validate
    super
  end

  def full_name
    [given, family].compact.join(' ')
  end

  def formal_name
    [family.upcase, given].compact.join(', ')
  end

  def aliased
    if aliased_full_name then
      CdC::Reviewer.by_name(aliased_full_name)
    end
  end

  def slug
    [given, family]
      .compact
      .map { |word| word.gsub(/’|\./, '') }
      .map { |word| word.gsub(/ /, '-') }
      .map { |word| AnyAscii.transliterate(word).downcase }
      .join('-')
  end

  def natural_order
    AnyAscii.transliterate(
      [family_departicled, given]
        .compact
        .join(' ')
        .downcase
    )
  end

  private

  def family_departicled
    family.gsub(/^(de )|(du )|(d’)/, '')
  end

  class << self

    # this is fragile

    def by_name(name)
      by_given_and_family(name) || by_family_only(name)
    end

    private

    def by_given_and_family(name)
      find { printf('%s %s', given, family) =~ name }
    end

    def by_family_only(name)
      find(family: name)
    end

  end
end
