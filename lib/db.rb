require_relative '../config'

DB = Sequel.sqlite(cdc_path_db)
DB.run('PRAGMA synchronous = OFF')

def create_unless_exists(table, &block)
  unless DB.table_exists? table then
    DB.create_table(table, &block)
  end
end

create_unless_exists(:reviewers) do
  primary_key :id
  String :given, null: true
  String :family
  String :initials
  String :aliased_full_name, null: true
  String :imdb_id, null: true
end

create_unless_exists(:issues) do
  primary_key :number
  Integer :year, index: true
  Integer :month_first
  Integer :month_last
  String :file
end

create_unless_exists(:films) do
  primary_key :id
  String  :title
  String  :title_lang
  Boolean :title_definitive
  String  :director1
  String  :director2
  String  :director3
  String  :director4
end

create_unless_exists(:imdbs) do
  primary_key :id
  foreign_key :film_id, :films
  String :imdb_id, index: true
  String :episode, null: true
end

create_unless_exists(:aocs) do
  primary_key :id
  foreign_key :film_id, :films
  String :aoc_id, index: true
  String :aoc_media
  String :episode, null: true
end

create_unless_exists(:reviews) do
  primary_key :id
  foreign_key :number, :issues
  String :type
  Integer :page_first
  Integer :page_last
end

create_unless_exists(:film_reviews) do
  primary_ket :id
  foreign_key :film_id, :films
  foreign_key :review_id, :reviews
end

create_unless_exists(:reviewer_reviews) do
  primary_key :id
  foreign_key :reviewer_id, :reviewers
  foreign_key :review_id, :reviews
end
