#!/bin/sh

# This adds an <aoc> entry to each film record for which we
# know the AoC id (that's from EIDR).  It is not idempotent,
# if you run it again you will get duplicate <aoc>s, so don't
# do that, it is saved since we might want to do something
# similar at a later date ...

csv='var/imdb-aoc.csv'

if [ -e $csv ]
then
    echo "found $csv"
else
    echo "creating $csv"
    bin/cdc-ids --aoc > $csv
fi

echo "reading $csv"

while IFS="," read -r imdb aoc
do
    echo "$imdb/$aoc"

    files=$(git grep $imdb | grep src/issue | cut -d: -f1 | uniq)

    for file in $files
    do
        echo "- $file"
        xmlstarlet \
            ed -P -L \
            --append "/cdc:issue/film[imdb='$imdb']/imdb" \
            --type elem \
            --name aoc \
            --value $aoc \
            $file
        sed --in-place "s/imdb><aoc/imdb>\n    <aoc/g" $file
        xmlstarlet \
            ed -P -L \
            --append "/cdc:issue/review/film[imdb='$imdb']/imdb" \
            --type elem \
            --name aoc \
            --value $aoc \
            $file
        sed --in-place "s/imdb><aoc/imdb>\n      <aoc/g" $file
        sed --in-place "s/\"/'/g" $file
    done
done < $csv

exit
