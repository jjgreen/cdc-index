require 'spec_helper'

describe CdC::API do
  include Rack::Test::Methods

  def app
    CdC::API
  end

  describe 'GET /api/no-such-route' do
    before { get '/api/no-such-route' }

    it 'should return 404' do
      expect(last_response.status)
        .to eq 404
    end
  end

  describe 'GET /api/status' do
    before { get '/api/status' }

    it 'should return 200' do
      expect(last_response.status)
        .to eq 200
    end

    it 'should have JSON encoded "ok" in body' do
      expect(JSON.parse(last_response.body)['status'])
        .to eq 'ok'
    end
  end

  # tests on other routes will need a test db, TODO

end
