require "rack/test"
require_relative '../lib/api'

RSpec.configure do |config|
  config.color = true
  config.tty = true
  config.formatter = :documentation
end
