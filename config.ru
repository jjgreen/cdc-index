# -*- ruby -*-
require 'rack'
require 'rack/cors'
require 'sequel'
require_relative 'config'

require_cdc 'db'
require_cdc 'model'
require_cdc 'api'

use Rack::Cors do
  allow do
    origins('*')
    resource('*', headers: :any, methods: :get)
  end
end

run CdC::API
