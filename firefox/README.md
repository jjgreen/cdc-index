CdC-index web extension
-----------------------

This is a web-extension injecting data from the index API into
the relevant pages on IMDb and AlloCiné.

It is developed for Firefox initially, but _should_ be portable
to other browsers (in particular, Chrome) with minimal effort.
See [here][1] for further details on web extensions.

Added 2021:  I've recently been locked-out of my Firefox account
since access now requires a mobile phone (and I don't have or
want one), so this extension cannot be updated, and the IMDb
page-layout has changed, so the _Cahiers_ data is no longer
injected correctly.  The AlloCiné part is still working for now
though.

Added 2022: I've received another email from Firefox informing
me that the connection from the addon JavaScript to the server
which provides the reviews data must henceforth use HTTPS.  This
would be a trivial change were I to have access to my account,
but I don't.  So I expect the addon to be removed shortly.

[1]: https://developer.mozilla.org/en-US/Add-ons/WebExtensions
