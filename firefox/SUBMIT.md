Submitting a new version
------------------------

Once happy with the code and with `make lint-ext` passing, increment
the `version` value in the manifest.  Then run `make zip` to create
a zipfile which can be uploaded at

    https://addons.mozilla.org/en-US/developers/addon/cdc-index/edit

There is no need to sign the zipfile, that is done automatically on
approval.

One should run `make plugin-tag` from the parent directory before
making further commits (probably wise to not upload until this is
done, since it does some error checks).
