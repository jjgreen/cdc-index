/* exported apiUrl */
/* global getCahiers */

function apiUrl(imdbId) {
    return 'http://miles.shef.ac.uk/pub/cdc/api/imdb/' + imdbId;
}

function main() {
    var metas = document.getElementsByTagName('meta'), len = metas.length, i;
    for (i = 0 ; i < len ; i += 1) {
        if (metas[i].getAttribute('property') === 'pageId') {
            getCahiers(metas[i].getAttribute('content'));
            break;
        }
    }
}

main();
