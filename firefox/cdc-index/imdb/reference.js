/* exported addCahiers */
/* global makeIssueNodes */

function addCahiers(results) {

    var cahiersTitle = document.createTextNode('Cahiers du Cinéma: ');
    var cahiersUList = document.createElement('ul');
    cahiersUList.className = 'ipl-inline-list';
    var cahiersListItem = document.createElement('li');
    cahiersListItem.className = 'ipl-inline-list__item';

    if (results.length > 0) {
        makeIssueNodes(results, null).forEach(
            function(issueNode) {
                cahiersListItem.appendChild(issueNode);
            }
        );
    } else {
        cahiersListItem.textContent = 'Rien n’a été trouvé';
    }

    cahiersUList.appendChild(cahiersListItem);

    var cahiersItem = document.createElement('div');
    cahiersItem.className = 'titlereference-overview-section';

    cahiersItem.appendChild(cahiersTitle);
    cahiersItem.appendChild(cahiersUList);

    var overviewSections = document.getElementsByClassName('titlereference-section-overview');
    if (overviewSections.length !== 1) {
        var message = 'Found ' + overviewSections.length + 'titlereference-section-overview';
        throw new Error(message);
    }
    var overviewSection = overviewSections[0];

    overviewSection.appendChild(cahiersItem);
}
